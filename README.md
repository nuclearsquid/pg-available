# pg-available

A simple program that checks whether a PostgreSQL server is reachable and accepts queries.

Should work with any version of PostgreSQL 9.x and higher (including 10).

## NOTE

This is currently a toy project for me to learn Rust, and several things are still missing, like:

* Supporting more auth schemes (currently only no password, plain-text and MD5 authentication challenges are supported)
* Support TLS connections
* Support connecting via UNIX socket
* Support the [other standard environment variables](https://www.postgresql.org/docs/current/static/libpq-envars.html)
* More detailed error information
* More detailed exit codes
* And other stuff

Use it at your own risk.

## Usage

The simplest use case only requires the database name; all other values are derived from the environment:

```sh
$ pg-available test-db
Authentication successful
PostgreSQL is ready!
```

Additionally, `pg-available` sets the exit code to `0` if it was successfully able to connect, and to `1` if there was any kind of problem.

## Providing credentials

Just like the `psql` utility, you can use the `--host`, `--port` and `--username` arguments to set their respective values.

In order to provide the password, you must set the `PGPASSWORD` environment variable. `pg-available` does not prompt you for the password.
