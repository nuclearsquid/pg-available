use std::io::prelude::*;
use std::net::TcpStream;
use std::env;

extern crate users;
use users::{get_user_by_uid, get_current_uid};

extern crate byteorder;
use byteorder::{ByteOrder, BigEndian, ReadBytesExt, WriteBytesExt};

extern crate crypto;
use crypto::digest::Digest;
use crypto::md5::Md5;

extern crate clap;
use clap::App;

// def self.ready?(host:, port:, user:, password: nil, database: nil)
fn pg_ready(host: &str, port: &str, database: &str, user: &str, password: Option<String>) -> Result<(), &'static str> {
    let mut stream = TcpStream::connect(format!("{}:{}", host, port)).unwrap();
    stream.set_nonblocking(false).expect("set_nonblocking(false) didn't work");
    stream.set_nodelay(true).expect("set_nodelay(true) didn't work");

    stream.take_error().expect("No error was expected...");

    let startup_message = build_startup_message(user, database);
    // println!("Startup message: {:?}", startup_message);
    stream.write_all(startup_message.as_slice()).unwrap();

    parse_pg_response(&stream, user, password)
}

fn parse_pg_response(mut stream: &TcpStream, user: &str, password: Option<String>) -> Result<(), &'static str> {
    // let mut peek_buf = vec![0; 128];
    // let peek_len = stream.peek(&mut peek_buf).expect("peek failed");
    // println!("TcpStream peek ({:?}): {:?}", peek_len, String::from_utf8_lossy(&peek_buf));
    // stream.take_error().expect("No error was expected...");

    let char_tag = stream.read_u8().unwrap() as char;
    let msg_length = stream.read_u32::<BigEndian>().unwrap() - 4;
    let mut payload = vec![0; msg_length as usize];
    stream.read_exact(&mut payload).unwrap();

    // println!("Read msg length {:?} from TCP stream", msg_length);
    // println!("Parsing message: {:?}", char_tag);

    match char_tag {
        'E' => {
            // let error_msg_length = stream.read_u32::<BigEndian>().unwrap();
            // let mut error_msg_buf : Vec<u8> = Vec::with_capacity(error_msg_length as usize);
            // stream.read(&mut error_msg_buf).unwrap();
            // let error_msg = String::from_utf8(error_msg_buf.clone()).unwrap();
            // println!("Received ErrorResponse: {:?} ({:?}, {:?})", error_msg, error_msg_length, &error_msg_buf);
            Err("Received ErrorResponse")
        },
        'R' => {

            // let decoded = stream.read_u32::<BigEndian>().unwrap();
            let decoded = BigEndian::read_u32(&payload);

            match decoded {
                0 => {
                    // Authentication successful
                    println!("Authentication successful");
                    parse_pg_response(stream, user, password)
                },
                // Cleartext password
                3 => {
                    if password.is_none() {
                        return Err("Authentication challenge received, but no password provided")
                    }

                    let password = password.unwrap();

                    println!("Authenticating via cleartext password");
                    // 'p' tag
                    stream.write_u8(b'p').unwrap();

                    // Length is the password length plus
                    // 4 bytes for the length field
                    stream.write_u32::<BigEndian>(password.len() as u32 + 4).unwrap();
                    stream.write_all(&password.clone().into_bytes()).unwrap();
                    stream.write_u8(0).unwrap();

                    parse_pg_response(stream, user, Some(password))
                },
                // MD5 password
                5 => {
                    if password.is_none() {
                        return Err("Authentication challenge received, but no password provided")
                    }

                    let password = password.unwrap();

                    println!("Authenticating via MD5");
                    let _length = stream.read_u32::<BigEndian>().unwrap() - 4;

                    // payload = socket.read(length)

                    // Payload is the salt, consisting of the length of the salt, and the salt
                    // itself
                    let salt_length = stream.read_u32::<BigEndian>().unwrap();
                    let mut salt = Vec::with_capacity(salt_length as usize);
                    stream.read_u32_into::<BigEndian>(&mut salt).unwrap();

                    let mut md5 = Md5::new();

                    // hashed_pass = Digest::MD5.hexdigest([password, user].join)
                    md5.input(&password.clone().into_bytes());
                    md5.input(&user.to_string().into_bytes());
                    let hashed_pass = md5.result_str();
                    md5.reset();

                    // encoded_password = "md5" + Digest::MD5.hexdigest([hashed_pass, salt].join)
                    md5.input(b"md5");
                    md5.input(hashed_pass.as_bytes());
                    let mut encoded_password : Vec<u8> = Vec::with_capacity(md5.output_bytes());
                    md5.result(&mut encoded_password);

                    // socket.write([112, encoded_password.size + 5, encoded_password].pack("CL>Z*"))
                    // 'p' tag
                    stream.write_u8(b'p').unwrap();
                    stream.write_u32::<BigEndian>(encoded_password.len() as u32 + 5).unwrap();
                    stream.write_all(&encoded_password).unwrap();
                    // Zero-terminate string
                    stream.write_u8(0).unwrap();

                    parse_pg_response(stream, user, Some(password))
                },
                _ => {
                    println!("Received unknown  or unimplemented authentication method value: {:?}", decoded);
                    Err("Unknown authentication method value")
                }
            }
        },
        // Ready for Query
        'Z' => {
            // Terminate connection ('X' tag)
            stream.write_u8(88).expect("write failed");
            stream.write_u32::<BigEndian>(4).expect("write failed");
            Ok(())
        },
        _ => {
            parse_pg_response(stream, user, password)
        }
    }
}

fn build_startup_message(user: &str, database: &str) -> Vec<u8> {
    // message = [0, 196608]
    let mut message : Vec<u8> = vec![
        // Message size, will be set later
        0, 0, 0, 0,
        // Protocol version
        0, 3, 0, 0
    ];

    // message_size = 4 + 4
    let mut message_size : u32 = 4 + 4;

    // pack_string = "L>L>"

    // params = ["user", user]
    let mut params : Vec<String> = vec![String::from("user")];
    params.push(user.to_string());

    // params.concat(["database", database]) if database
    params.push(String::from("database"));
    params.push(database.to_string());

    // params.each do |value|
    //   message << value
    //   message_size += value.size + 1
    //   pack_string << "Z*"
    // end
    for value in params {
        let value_length = value.len() as u32;
        message.append(&mut value.into_bytes());
        // Nul-Terminate string
        message.push(0);
        message_size += value_length + 1;
    }

    // message << 0
    // message_size += 1
    // pack_string << "C"
    message.push(0);
    message_size += 1;

    // message[0] = message_size
    let message_size_bytes : [u8; 4] = unsafe {std::mem::transmute(message_size)};
    message[0] = message_size_bytes[3];
    message[1] = message_size_bytes[2];
    message[2] = message_size_bytes[1];
    message[3] = message_size_bytes[0];

    message
}


fn main() {
    let matches = App::new("pg-available")
        .version("0.1")
        .author("Markus Wein <markus.wein@nuclearsquid.com>")
        .about("Checks if a PostgreSQL server is up and available for queries")
        .args_from_usage(
          r#"-h, --host=[HOSTNAME]     'database server host (default: "localhost")'
             -p, --port=[PORT]         'database server port (default: "5432")'
             -U, --username=[USERNAME] 'database user name (defaults to current logged in user)'
             <DBNAME>                  'Database to connect to'"#)
        .get_matches();

    // TODO: Support UNIX sockets
    let host = matches.value_of("host").unwrap_or("localhost");
    let port = matches.value_of("port").unwrap_or("5432");
    let current_user = get_user_by_uid(get_current_uid()).unwrap();
    let username = matches.value_of("username").unwrap_or_else(|| current_user.name());
    let password = env::var("PGPASSWORD").ok();
    let dbname = matches.value_of("DBNAME").unwrap();

    // let host = SocketAddr::from(format!("{}:{}", host, port));
    let pg_readiness = pg_ready(host, port, dbname, username, password);

    match pg_readiness {
        Ok(()) => println!("PostgreSQL is ready!"),
        Err(e) => {
            println!("PostgreSQL is not ready: {:?}", e);
            std::process::exit(1);
        },
    }
}
